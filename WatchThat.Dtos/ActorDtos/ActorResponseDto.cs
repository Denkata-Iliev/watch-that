﻿namespace WatchThat.Dtos.ActorDtos
{
    public class ActorResponseDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int YearOfBirth { get; set; }

        public int Age { get; set; }
    }
}
