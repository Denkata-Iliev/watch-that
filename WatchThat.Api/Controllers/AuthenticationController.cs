﻿using Microsoft.AspNetCore.Mvc;
using WatchThat.Dtos.Authentication;
using WatchThat.Service.AuthenticationService;

namespace WatchThat.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly IAuthenticationService authService;

        public AuthenticationController(IAuthenticationService authService)
        {
            this.authService = authService;
        }

        [HttpPost("register")]
        public IActionResult Register([FromBody] RegisterDto register)
        {
            return Ok(authService.Register(register));
        }

        [HttpPost("login")]
        public IActionResult Login([FromBody] LoginDto login)
        {
            return Ok(authService.Login(login));
        }
    }
}
