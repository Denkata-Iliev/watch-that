﻿using WatchThat.Dtos;
using WatchThat.Dtos.ActorDtos;
using WatchThat.Entity;
using WatchThat.Service.BaseService;

namespace WatchThat.Service.ActorService
{
    public interface IActorService : IBaseService<Actor>
    {
        ActorResponseDto Create(ActorCreateDto createDto);

        ActorResponseDto Update(ActorCreateDto dto, int id);

        new PaginationDto<ActorResponseDto> GetPage(int page, int size);

        new ActorResponseDto GetById(int id);

        Actor GetEntityById(int id);
    }
}
