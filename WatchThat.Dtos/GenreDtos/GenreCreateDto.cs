﻿using System.ComponentModel.DataAnnotations;

namespace WatchThat.Dtos.GenreDtos
{
    public class GenreCreateDto
    {
        [Required]
        [MinLength(3, ErrorMessage = "Name must be at least 3 characters long")]
        public string Name { get; set; }
    }
}
