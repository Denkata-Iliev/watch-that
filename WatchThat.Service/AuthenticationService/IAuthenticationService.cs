﻿using WatchThat.Dtos.Authentication;

namespace WatchThat.Service.AuthenticationService
{
    public interface IAuthenticationService
    {
        UserResponseDto Register(RegisterDto register);

        LoginResponseDto Login(LoginDto login);
    }
}
