﻿using CsvHelper;
using System.Globalization;
using WatchThat.Entity;

namespace WatchThat.Repository
{
    public static class DatabaseSeeder
    {
        public static void SeedDb(WatchThatContext context)
        {
            SeedGenres(context);
            SeedDirectors(context);
            SeedActors(context);
            SeedMovies(context);
        }

        private static void SeedGenres(WatchThatContext context)
        {
            if (context.Genres.Any())
            {
                return;
            }

            using var reader = new StreamReader("MovieGenre.csv");
            using var csv = new CsvReader(reader, CultureInfo.InvariantCulture);

            csv.Read();
            csv.ReadHeader();

            while (csv.Read())
            {
                string[] genreNames = csv.GetField("Genre").Split("|");

                foreach (string name in genreNames)
                {
                    if (string.IsNullOrEmpty(name) || context.Genres.Any(g => g.Name == name))
                    {
                        continue;
                    }

                    Genre genre = new Genre(name);
                    context.Genres.Add(genre);
                    context.SaveChanges();
                }
            }
        }

        private static void SeedDirectors(WatchThatContext context)
        {
            if (context.Directors.Any())
            {
                return;
            }

            using var reader = new StreamReader("netflix_titles.csv");
            using var csv = new CsvReader(reader, CultureInfo.InvariantCulture);

            csv.Read();
            csv.ReadHeader();

            while (csv.Read())
            {
                string[] directorNames = csv.GetField("director").Split(",");
                foreach (string name in directorNames)
                {
                    if (string.IsNullOrEmpty(name) || context.Directors.Any(d => d.Name == name))
                    {
                        continue;
                    }

                    Director director = new Director(name, new Random().Next(1950, DateTime.Now.Year - 16));
                    context.Directors.Add(director);
                    context.SaveChanges();
                }
            }
        }

        private static void SeedActors(WatchThatContext context)
        {
            if (context.Actors.Any())
            {
                return;
            }

            using var reader = new StreamReader("actors.csv");
            using var csv = new CsvReader(reader, CultureInfo.InvariantCulture);

            csv.Read();
            csv.ReadHeader();

            while (csv.Read())
            {
                string actorName = csv.GetField("Actor");
                if (context.Actors.Any(a => a.Name == actorName))
                {
                    continue;
                }

                int randomBirthYear = new Random().Next(1950, DateTime.Now.Year - 16);
                Actor actor = new Actor(actorName, randomBirthYear);

                context.Actors.Add(actor);
                context.SaveChanges();
            }
        }

        private static void SeedMovies(WatchThatContext context)
        {
            if (context.Movies.Any())
            {
                return;
            }

            using var reader = new StreamReader("netflix_titles.csv");
            using var csv = new CsvReader(reader, CultureInfo.InvariantCulture);

            csv.Read();
            csv.ReadHeader();

            while (csv.Read())
            {
                if (csv.GetField("type") != "Movie")
                {
                    continue;
                }

                Movie movie = new Movie(csv.GetField("title"))
                {
                    Budget = new Random().Next() * 0.11m,
                    Earnings = new Random().Next() * 2.11m,
                    PublishedYear = csv.GetField<int>("release_year"),
                    Director = GetRandomDirector(context.Directors.ToList()),
                    Cast = GetRandomActors(context.Actors.ToList()),
                    Genres = GetRandomGenres(context.Genres.ToList()),
                    Duration = GetDuration(csv.GetField("duration").Split(" ")[0])
                };

                context.Movies.Add(movie);
            }

            context.SaveChanges();
        }

        private static int GetDuration(string v)
        {
            if (int.TryParse(v, out int parsed))
            {
                return parsed;
            }

            return new Random().Next(60, 300);
        }

        private static ISet<Genre> GetRandomGenres(List<Genre> genres)
        {
            int genresNumber = new Random().Next(1, 4);
            ISet<Genre> randomGenres = new HashSet<Genre>();

            for (int i = 1; i <= genresNumber; i++)
            {
                int randomIndex = new Random().Next(0, genres.Count);
                randomGenres.Add(genres[randomIndex]);
            }

            return randomGenres;
        }

        private static ISet<Actor> GetRandomActors(List<Actor> actors)
        {
            int actorsNumber = new Random().Next(1, 6);
            ISet<Actor> randomActors = new HashSet<Actor>();

            for (int i = 1; i <= actorsNumber; i++)
            {
                int randomIndex = new Random().Next(0, actors.Count);
                randomActors.Add(actors[randomIndex]);
            }

            return randomActors;
        }

        private static Director GetRandomDirector(List<Director> directors)
        {
            int randomIndex = new Random().Next(0, directors.Count);

            return directors[randomIndex];
        }
    }
}
