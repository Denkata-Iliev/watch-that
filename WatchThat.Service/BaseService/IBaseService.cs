﻿using WatchThat.Entity;

namespace WatchThat.Service.BaseService
{
    public interface IBaseService<T> where T : BaseEntity
    {
        T GetById(int id);

        IEnumerable<T> GetPage(int page, int size);

        int GetCount();

        void Create(T entity);

        void Update(T entity);

        void Delete(int id);
    }
}
