﻿using Microsoft.EntityFrameworkCore;
using WatchThat.Entity;
using WatchThat.Repository.BaseRepository;

namespace WatchThat.Repository.DirectorRepository
{
    public class DirectorRepositoryImpl : BaseRepositoryImpl<Director>, IDirectorRepository
    {
        private readonly DbSet<Director> directors;
        public DirectorRepositoryImpl(WatchThatContext context) : base(context)
        {
            directors = context.Set<Director>();
        }
    }
}
