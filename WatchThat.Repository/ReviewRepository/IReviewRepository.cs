﻿using WatchThat.Entity;
using WatchThat.Repository.BaseRepository;

namespace WatchThat.Repository.ReviewRepository
{
    public interface IReviewRepository : IBaseRepository<Review>
    {
    }
}
