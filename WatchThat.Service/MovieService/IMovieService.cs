﻿using WatchThat.Dtos;
using WatchThat.Dtos.MovieDtos;
using WatchThat.Entity;
using WatchThat.Service.BaseService;

namespace WatchThat.Service.MovieService
{
    public interface IMovieService : IBaseService<Movie>
    {
        MovieResponseDto Create(MovieCreateDto createDto);

        Movie GetEntityById(int id);

        new MovieResponseDto GetById(int id);

        PaginationDto<MovieResponseDto> GetMoviesByName(string name, int page, int size);

        PaginationDto<MovieResponseDto> GetMoviesByGenre(string genreName, int page, int size);

        new PaginationDto<MovieResponseDto> GetPage(int page, int size);

        MovieResponseDto Update(int id, MovieCreateDto dto);
    }
}
