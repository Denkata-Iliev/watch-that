﻿using WatchThat.Entity;

namespace WatchThat.Repository.BaseRepository
{
    public interface IBaseRepository<T> where T : BaseEntity
    {
        T GetById(int id);

        ICollection<T> GetAll();

        IEnumerable<T> GetPage(int page, int size);

        int GetCount();

        void Create(T entity);

        void Update(T entity);

        void Delete(T entity);
    }
}
