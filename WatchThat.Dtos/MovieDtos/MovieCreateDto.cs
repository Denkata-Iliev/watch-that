﻿using System.ComponentModel.DataAnnotations;

namespace WatchThat.Dtos.MovieDtos
{
    public class MovieCreateDto
    {
        [Required]
        [MinLength(3, ErrorMessage = "Name must be at least 3 characters long")]
        public string Name { get; set; }

        [Required]
        [Range(1, 999999999, ErrorMessage = "Budget must be a positive number")]
        public decimal Budget { get; set; }

        [Required]
        [Range(1, 999999999, ErrorMessage = "Earnings must be a positive number")]
        public decimal Earnings { get; set; }

        [Required]
        [Range(1, 9999, ErrorMessage = "Published year must be a positive number")]
        public int PublishedYear { get; set; }

        [Required]
        [Range(1, 300, ErrorMessage = "Duration must be a value between 1 and 300")]
        public int Duration { get; set; }

        [Required]
        public int DirectorId { get; set; }

        [Required]
        [MinLength(1, ErrorMessage = "CastIds must have at least one element")]
        public ISet<int> CastIds { get; set; }

        [Required]
        [MinLength(1, ErrorMessage = "GenreIds must have at least one element")]
        public ISet<int> GenreIds { get; set; }
    }
}
