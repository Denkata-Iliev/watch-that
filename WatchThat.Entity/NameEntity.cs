﻿namespace WatchThat.Entity
{
    public abstract class NameEntity : BaseEntity
    {
        public string Name { get; set; }

        public NameEntity(string name)
        {
            Name = name;
        }
    }
}
