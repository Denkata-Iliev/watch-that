﻿using WatchThat.Entity;
using WatchThat.Repository.BaseRepository;

namespace WatchThat.Repository.ActorRepository
{
    public interface IActorRepository : IBaseRepository<Actor>
    {
    }
}
