﻿using WatchThat.Dtos;
using WatchThat.Dtos.GenreDtos;
using WatchThat.Entity;
using WatchThat.Service.BaseService;

namespace WatchThat.Service.GenreService
{
    public interface IGenreService : IBaseService<Genre>
    {
        GenreResponseDto Create(GenreCreateDto createDto);

        new GenreResponseDto GetById(int id);

        new PaginationDto<GenreResponseDto> GetPage(int page, int size);

        GenreResponseDto Update(int id, GenreCreateDto dto);

        Genre GetEntityById(int id);

        Genre GetEntityByName(string name);
    }
}
