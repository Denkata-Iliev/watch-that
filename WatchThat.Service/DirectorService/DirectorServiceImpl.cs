﻿using AutoMapper;
using LazyCache;
using WatchThat.Dtos;
using WatchThat.Dtos.DirectorDtos;
using WatchThat.Entity;
using WatchThat.Repository.DirectorRepository;
using WatchThat.Service.BaseService;

namespace WatchThat.Service.DirectorService
{
    public class DirectorServiceImpl : BaseServiceImpl<Director>, IDirectorService
    {
        private readonly IDirectorRepository directorRepository;
        private readonly IMapper mapper;

        public DirectorServiceImpl(IDirectorRepository directorRepository,
                                    IMapper mapper,
                                    IAppCache cache) : base(directorRepository, cache)
        {
            this.directorRepository = directorRepository;
            this.mapper = mapper;
        }

        public DirectorResponseDto Create(DirectorCreateDto createDto)
        {
            Director director = mapper.Map<Director>(createDto);

            Create(director);

            return mapper.Map<DirectorResponseDto>(director);
        }

        public new PaginationDto<DirectorResponseDto> GetPage(int page, int size)
        {
            decimal decimalSize = Convert.ToDecimal(size);
            int totalElements = GetCount();
            PaginationDto<DirectorResponseDto> pagination = new PaginationDto<DirectorResponseDto>
            {
                Page = page,
                Size = size,
                Content = base.GetPage(page, size).Select(mapper.Map<DirectorResponseDto>),
                PageCount = Math.Ceiling(totalElements / decimalSize),
                TotalElements = totalElements
            };

            return pagination;
        }

        public new DirectorResponseDto GetById(int id)
        {
            Director director = base.GetById(id);
            return mapper.Map<DirectorResponseDto>(director);
        }

        public DirectorResponseDto Update(DirectorCreateDto dto, int id)
        {
            Director director = base.GetById(id);

            mapper.Map(dto, director);
            director.Age = DateTime.Now.Year - director.YearOfBirth;

            Update(director);

            return mapper.Map<DirectorResponseDto>(director);
        }

        public Director GetEntityById(int id)
        {
            return base.GetById(id);
        }
    }
}
