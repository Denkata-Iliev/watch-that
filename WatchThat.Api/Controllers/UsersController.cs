﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WatchThat.Dtos.Authentication;
using WatchThat.Dtos.UserDtos;
using WatchThat.Service.UserService;

namespace WatchThat.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UsersController : ControllerBase
    {

        private readonly IUserService userService;

        public UsersController(IUserService userService)
        {
            this.userService = userService;
        }

        [HttpPost]
        [Authorize(Roles = "ADMIN")]
        public IActionResult CreateAdmin([FromBody] RegisterDto register)
        {
            return Ok(userService.CreateAdmin(register));
        }

        [HttpGet]
        [Authorize(Roles = "ADMIN")]
        public IActionResult GetAll([FromQuery] int page = 1, [FromQuery] int size = 5)
        {
            return Ok(userService.GetPage(page, size));
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            return Ok(userService.GetById(id));
        }

        [HttpPatch("{username}")]
        public IActionResult Update([FromRoute] string username, [FromBody] UserUpdateDto updateDto)
        {
            return Ok(userService.Update(updateDto, username));
        }

        [HttpPatch("{username}/role")]
        [Authorize(Roles = "ADMIN")]
        public IActionResult ChangeRole([FromRoute] string username, [FromBody] RoleChangeDto roleChange)
        {
            return Ok(userService.ChangeRole(username, roleChange));
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "ADMIN")]
        public IActionResult Delete([FromRoute] int id)
        {
            userService.Delete(id);
            return NoContent();
        }
    }
}
