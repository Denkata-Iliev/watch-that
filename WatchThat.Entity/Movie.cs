﻿namespace WatchThat.Entity
{
    public class Movie : NameEntity
    {
        public decimal Budget { get; set; }

        public decimal Earnings { get; set; }

        public int PublishedYear { get; set; }

        public int Duration { get; set; }

        public int DirectorId { get; set; }
        public Director Director { get; set; }

        public ISet<Actor> Cast { get; set; } = new HashSet<Actor>();

        public ISet<Genre> Genres { get; set; } = new HashSet<Genre>();

        public ISet<Review> Reviews { get; set; } = new HashSet<Review>();

        public Movie(string name) : base(name)
        {
        }
    }
}
