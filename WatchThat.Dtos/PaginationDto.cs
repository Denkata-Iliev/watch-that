﻿namespace WatchThat.Dtos
{
    public class PaginationDto<T>
    {
        public int Page { get; set; }

        public int Size { get; set; }

        public IEnumerable<T> Content { get; set; } = new List<T>();

        public decimal PageCount { get; set; }

        public int TotalElements { get; set; }
    }
}
