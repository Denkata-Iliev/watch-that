﻿using WatchThat.Entity;

namespace WatchThat.Dtos.Authentication
{
    public class UserResponseDto
    {
        public int Id { get; set; }

        public string Username { get; set; }

        public Role Role { get; set; }
    }
}
