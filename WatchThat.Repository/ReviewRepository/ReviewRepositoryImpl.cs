﻿using Microsoft.EntityFrameworkCore;
using WatchThat.Entity;
using WatchThat.Repository.BaseRepository;

namespace WatchThat.Repository.ReviewRepository
{
    public class ReviewRepositoryImpl : BaseRepositoryImpl<Review>, IReviewRepository
    {
        private readonly DbSet<Review> reviews;

        public ReviewRepositoryImpl(WatchThatContext context) : base(context)
        {
            reviews = context.Set<Review>();
        }
    }
}
