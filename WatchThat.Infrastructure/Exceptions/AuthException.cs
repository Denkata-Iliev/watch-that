﻿using System.Net;

namespace WatchThat.Infrastructure.Exceptions
{
    public class AuthException : CustomHttpException
    {
        public AuthException(string? message, HttpStatusCode statusCode) : base(message, statusCode)
        {
        }
    }
}
