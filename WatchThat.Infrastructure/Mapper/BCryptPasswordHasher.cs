﻿using AutoMapper;
using WatchThat.Dtos.Authentication;
using WatchThat.Dtos.UserDtos;
using WatchThat.Entity;

namespace WatchThat.Infrastructure.Mapper
{
    public class BCryptPasswordHasher : ITypeConverter<RegisterDto, User>, ITypeConverter<UserUpdateDto, User>
    {
        public User Convert(RegisterDto source, User destination, ResolutionContext context)
        {
            return new User
            {
                Username = source.Username,
                Password = BCrypt.Net.BCrypt.HashPassword(source.Password)
            };
        }

        public User Convert(UserUpdateDto source, User destination, ResolutionContext context)
        {
            destination.Password = BCrypt.Net.BCrypt.HashPassword(source.NewPassword);
            return destination;
        }
    }
}
