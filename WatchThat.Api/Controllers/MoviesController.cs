﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WatchThat.Dtos.MovieDtos;
using WatchThat.Service.MovieService;

namespace WatchThat.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly IMovieService movieService;

        public MoviesController(IMovieService movieService)
        {
            this.movieService = movieService;
        }

        [HttpPost]
        [Authorize(Roles = "ADMIN")]
        public IActionResult Create([FromBody] MovieCreateDto createDto)
        {
            return Ok(movieService.Create(createDto));
        }

        [HttpGet]
        [Authorize]
        public IActionResult GetPage([FromQuery] int page = 1, [FromQuery] int size = 5)
        {
            return Ok(movieService.GetPage(page, size));
        }

        [HttpGet("{id}")]
        [Authorize]
        public IActionResult GetById([FromRoute] int id)
        {
            return Ok(movieService.GetById(id));
        }

        [HttpGet("getByName")]
        [Authorize]
        public IActionResult GetByName([FromForm] string name, [FromQuery] int page = 1, [FromQuery] int size = 5)
        {
            return Ok(movieService.GetMoviesByName(name, page, size));
        }

        [HttpGet("getByGenre")]
        [Authorize]
        public IActionResult GetByGenre([FromHeader] string genreName, [FromQuery] int page = 1, [FromQuery] int size = 5)
        {
            return Ok(movieService.GetMoviesByGenre(genreName, page, size));
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "ADMIN")]
        public IActionResult Update([FromRoute] int id, [FromBody] MovieCreateDto dto)
        {
            return Ok(movieService.Update(id, dto));
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "ADMIN")]
        public IActionResult Delete([FromRoute] int id)
        {
            movieService.Delete(id);
            return NoContent();
        }
    }
}
