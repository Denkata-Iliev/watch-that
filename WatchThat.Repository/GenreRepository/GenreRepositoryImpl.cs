﻿using Microsoft.EntityFrameworkCore;
using WatchThat.Entity;
using WatchThat.Repository.BaseRepository;

namespace WatchThat.Repository.GenreRepository
{
    public class GenreRepositoryImpl : BaseRepositoryImpl<Genre>, IGenreRepository
    {
        private readonly DbSet<Genre> genres;

        public GenreRepositoryImpl(WatchThatContext context) : base(context)
        {
            genres = context.Set<Genre>();
        }

        public bool ExistsByName(string name)
        {
            return genres.Any(g => g.Name == name);
        }
    }
}
