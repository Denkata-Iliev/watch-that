﻿using System.Net;

namespace WatchThat.Infrastructure.Exceptions
{
    public class ResourceNotFoundException : CustomHttpException
    {
        public ResourceNotFoundException(string? message) : base(message, HttpStatusCode.NotFound)
        {
        }
    }
}
