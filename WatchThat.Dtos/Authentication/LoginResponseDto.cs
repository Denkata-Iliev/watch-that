﻿namespace WatchThat.Dtos.Authentication
{
    public class LoginResponseDto
    {
        public string JwtToken { get; }

        public LoginResponseDto(string jwtToken)
        {
            JwtToken = jwtToken;
        }
    }
}
