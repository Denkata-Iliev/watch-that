﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WatchThat.Dtos.DirectorDtos;
using WatchThat.Service.DirectorService;

namespace WatchThat.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DirectorsController : ControllerBase
    {
        private readonly IDirectorService directorService;

        public DirectorsController(IDirectorService directorService)
        {
            this.directorService = directorService;
        }

        [HttpPost]
        [Authorize(Roles = "ADMIN")]
        public IActionResult Create([FromBody] DirectorCreateDto createDto)
        {
            return Ok(directorService.Create(createDto));
        }

        [HttpGet]
        [Authorize]
        public IActionResult GetAll([FromQuery] int page = 1, [FromQuery] int size = 5)
        {
            return Ok(directorService.GetPage(page, size));
        }

        [HttpGet("{id}")]
        [Authorize]
        public IActionResult Get([FromRoute] int id)
        {
            return Ok(directorService.GetById(id));
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "ADMIN")]
        public IActionResult Update([FromRoute] int id, [FromBody] DirectorCreateDto createDto)
        {
            return Ok(directorService.Update(createDto, id));
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "ADMIN")]
        public IActionResult Delete([FromRoute] int id)
        {
            directorService.Delete(id);
            return NoContent();
        }
    }
}
