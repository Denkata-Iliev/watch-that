﻿using Microsoft.EntityFrameworkCore;
using WatchThat.Entity;
using WatchThat.Repository.BaseRepository;

namespace WatchThat.Repository.UserRepository
{
    public class UserRepositoryImpl : BaseRepositoryImpl<User>, IUserRepository
    {
        private readonly DbSet<User> users;

        public UserRepositoryImpl(WatchThatContext context) : base(context)
        {
            users = context.Set<User>();
        }

        public bool ExistsByUsername(string username)
        {
            return users.Any(u => u.Username == username);
        }

        public bool IsLastAdmin()
        {
            return users.Count(u => u.Role == Role.ADMIN) == 1;
        }

        public User GetByUsername(string username)
        {
            return users.First(u => u.Username == username);
        }
    }
}
