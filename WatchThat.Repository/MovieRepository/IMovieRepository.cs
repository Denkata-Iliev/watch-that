﻿using WatchThat.Entity;
using WatchThat.Repository.BaseRepository;

namespace WatchThat.Repository.MovieRepository
{
    public interface IMovieRepository : IBaseRepository<Movie>
    {
    }
}
