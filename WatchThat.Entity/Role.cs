﻿using System.Text.Json.Serialization;

namespace WatchThat.Entity
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum Role
    {
        USER,
        ADMIN
    }
}
