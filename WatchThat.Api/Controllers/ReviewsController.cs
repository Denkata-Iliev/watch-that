﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WatchThat.Dtos.ReviewDtos;
using WatchThat.Service.ReviewService;

namespace WatchThat.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ReviewsController : ControllerBase
    {
        private readonly IReviewService reviewService;

        public ReviewsController(IReviewService reviewService)
        {
            this.reviewService = reviewService;
        }

        [HttpPost]
        public IActionResult Create([FromBody] ReviewCreateDto reviewCreateDto)
        {
            return Ok(reviewService.Create(reviewCreateDto));
        }

        [HttpGet]
        public IActionResult GetPage([FromQuery] int page = 1, [FromQuery] int size = 5)
        {
            return Ok(reviewService.GetPage(page, size));
        }

        [HttpGet("{id}")]
        public IActionResult GetById([FromRoute] int id)
        {
            return Ok(reviewService.GetById(id));
        }

        [HttpPatch("{id}")]
        public IActionResult Update([FromRoute] int id, [FromBody] ReviewUpdateDto reviewUpdateDto)
        {
            return Ok(reviewService.Update(id, reviewUpdateDto));
        }

        [HttpDelete("{id}")]
        public IActionResult Delete([FromRoute] int id)
        {
            reviewService.Delete(id);
            return NoContent();
        }
    }
}
