﻿using DinkToPdf;
using DinkToPdf.Contracts;
using WatchThat.Entity;
using WatchThat.Service.ActorService;
using WatchThat.Service.DirectorService;
using WatchThat.Service.GenreService;
using WatchThat.Service.MovieService;
using WatchThat.Service.ReviewService;
using WatchThat.Service.UserService;

namespace WatchThat.Service.PdfService
{
    public class PdfServiceImpl : IPdfService
    {
        private readonly IConverter converter;
        private readonly IMovieService movieService;
        private readonly IGenreService genreService;
        private readonly IActorService actorService;
        private readonly IDirectorService directorService;
        private readonly IReviewService reviewService;
        private readonly IUserService userService;

        public PdfServiceImpl(IConverter converter,
                             IMovieService movieService,
                             IGenreService genreService,
                             IActorService actorService,
                             IDirectorService directorService,
                             IReviewService reviewService,
                             IUserService userService)
        {
            this.converter = converter;
            this.movieService = movieService;
            this.genreService = genreService;
            this.actorService = actorService;
            this.directorService = directorService;
            this.reviewService = reviewService;
            this.userService = userService;
        }
        public void CreatePdf()
        {
            GlobalSettings globalSettings = new GlobalSettings
            {
                ColorMode = ColorMode.Color,
                Orientation = Orientation.Portrait,
                PaperSize = PaperKind.A4,
                Margins = new MarginSettings { Top = 10 },
                DocumentTitle = "PDF Report",
                Out = "Report.pdf"
            };

            ObjectSettings objectSettings = new ObjectSettings
            {
                PagesCount = true,
                HtmlContent = GetHTMLString(),
                WebSettings = { DefaultEncoding = "utf-8" },
                HeaderSettings = { FontName = "Arial" },
                FooterSettings = { FontName = "Arial" }
            };

            HtmlToPdfDocument pdf = new HtmlToPdfDocument()
            {
                GlobalSettings = globalSettings,
                Objects = { objectSettings }
            };

            converter.Convert(pdf);
        }

        private string GetHTMLString()
        {
            return

$@"<html>
    <head>
        <style>
            .header {{
                text-align: center;
                color: green;
                padding-bottom: 35px;
            }}

            table {{
                width: 80%;
                border-collapse: collapse;
                margin: 0 auto;
            }}

            td, th {{
                border: 1px solid gray;
                padding: 15px;
                font-size: 22px;
                text-align: center;
            }}

            table th {{
                background-color: green;
                color: white;
            }}
        </style>
    </head>
    <body>
        <div class='header'>
            <h1>PDF Report</h1>
        </div>
        <table>
            <thead>
                <th>Entity Name</th>
                <th>Count</th>
            </thead>
            <tr>
                <td>{nameof(Movie)}</td>
                <td>{movieService.GetCount()}</td>
            </tr>
            <tr>
                <td>{nameof(Director)}</td>
                <td>{directorService.GetCount()}</td>
            </tr>
            <tr>
                <td>{nameof(Actor)}</td>
                <td>{actorService.GetCount()}</td>
            </tr>
            <tr>
                <td>{nameof(Genre)}</td>
                <td>{genreService.GetCount()}</td>
            </tr>
            <tr>
                <td>{nameof(Review)}</td>
                <td>{reviewService.GetCount()}</td>
            </tr>
            <tr>
                <td>{nameof(Entity.User)}</td>
                <td>{userService.GetCount()}</td>
            </tr>
        </table>
    </body>
</html>";
        }
    }
}
