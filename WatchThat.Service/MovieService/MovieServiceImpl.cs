﻿using AutoMapper;
using LazyCache;
using System.Drawing;
using WatchThat.Dtos;
using WatchThat.Dtos.MovieDtos;
using WatchThat.Entity;
using WatchThat.Repository.MovieRepository;
using WatchThat.Service.ActorService;
using WatchThat.Service.BaseService;
using WatchThat.Service.DirectorService;
using WatchThat.Service.GenreService;

namespace WatchThat.Service.MovieService
{
    public class MovieServiceImpl : BaseServiceImpl<Movie>, IMovieService
    {
        private readonly IMovieRepository movieRepository;
        private readonly IMapper mapper;
        private readonly IGenreService genreService;
        private readonly IDirectorService directorService;
        private readonly IActorService actorService;

        public MovieServiceImpl(IMovieRepository movieRepository,
                                IAppCache cache,
                                IMapper mapper,
                                IGenreService genreService,
                                IDirectorService directorService,
                                IActorService actorService) : base(movieRepository, cache)
        {
            this.movieRepository = movieRepository;
            this.mapper = mapper;
            this.genreService = genreService;
            this.directorService = directorService;
            this.actorService = actorService;
        }

        public MovieResponseDto Create(MovieCreateDto createDto)
        {
            Movie movie = mapper.Map<Movie>(createDto);

            SetMovieDirector(createDto.DirectorId, movie);

            SetMovieCast(createDto.CastIds, movie);

            SetMovieGenres(createDto.GenreIds, movie);

            Create(movie);

            return mapper.Map<MovieResponseDto>(movie);
        }

        public Movie GetEntityById(int id)
        {
            return base.GetById(id);
        }

        public PaginationDto<MovieResponseDto> GetMoviesByName(string name, int page, int size)
        {
            var movies = movieRepository.GetAll()
                .Where(m => m.Name.ToLower().Contains(name.ToLower()))
                .OrderBy(m => m.Name)
                .Select(mapper.Map<MovieResponseDto>);

            var moviesForPage = movies
                .Skip((page - 1) * size)
                .Take(size);

            PaginationDto<MovieResponseDto> pagination = new PaginationDto<MovieResponseDto>
            {
                Page = page,
                Size = size,
                Content = moviesForPage,
                PageCount = Math.Ceiling(movies.Count() / Convert.ToDecimal(size)),
                TotalElements = movies.Count()
            };

            return pagination;
        }

        public PaginationDto<MovieResponseDto> GetMoviesByGenre(string genreName, int page, int size)
        {
            Genre genre = genreService.GetEntityByName(genreName);

            var movies = movieRepository.GetAll()
                .Where(m => m.Genres.Contains(genre))
                .OrderByDescending(m => m.Duration)
                .Select(mapper.Map<MovieResponseDto>);

            var moviesForPage = movies
                .Skip((page - 1) * size)
                .Take(size);

            PaginationDto<MovieResponseDto> pagination = new PaginationDto<MovieResponseDto>
            {
                Page = page,
                Size = size,
                Content = moviesForPage,
                PageCount = Math.Ceiling(movies.Count() / Convert.ToDecimal(size)),
                TotalElements = movies.Count()
            };

            return pagination;
        }

        public new MovieResponseDto GetById(int id)
        {
            return mapper.Map<MovieResponseDto>(base.GetById(id));
        }

        public new PaginationDto<MovieResponseDto> GetPage(int page, int size)
        {
            decimal decimalSize = Convert.ToDecimal(size);
            int totalElements = GetCount();
            PaginationDto<MovieResponseDto> pagination = new PaginationDto<MovieResponseDto>
            {
                Page = page,
                Size = size,
                Content = base.GetPage(page, size).Select(mapper.Map<MovieResponseDto>),
                PageCount = Math.Ceiling(totalElements / decimalSize),
                TotalElements = totalElements
            };

            return pagination;
        }

        public MovieResponseDto Update(int id, MovieCreateDto dto)
        {
            Movie movie = base.GetById(id);

            mapper.Map(dto, movie);

            SetMovieDirector(dto.DirectorId, movie);

            SetMovieCast(dto.CastIds, movie);

            SetMovieGenres(dto.GenreIds, movie);

            Update(movie);

            return mapper.Map<MovieResponseDto>(movie);
        }

        private void SetMovieGenres(ISet<int> genreIds, Movie movie)
        {
            ISet<Genre> genres = new HashSet<Genre>();
            foreach (int id in genreIds)
            {
                genres.Add(genreService.GetEntityById(id));
            }

            movie.Genres = genres;
        }

        private void SetMovieCast(ISet<int> castIds, Movie movie)
        {
            ISet<Actor> actors = new HashSet<Actor>();
            foreach (int id in castIds)
            {
                actors.Add(actorService.GetEntityById(id));
            }

            movie.Cast = actors;
        }

        private void SetMovieDirector(int directorId, Movie movie)
        {
            movie.Director = directorService.GetEntityById(directorId);
        }
    }
}
