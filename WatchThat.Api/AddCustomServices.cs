﻿using WatchThat.Repository.ActorRepository;
using WatchThat.Repository.DirectorRepository;
using WatchThat.Repository.GenreRepository;
using WatchThat.Repository.MovieRepository;
using WatchThat.Repository.UserRepository;
using WatchThat.Service.ActorService;
using WatchThat.Service.AuthenticationService;
using WatchThat.Service.DirectorService;
using WatchThat.Service.GenreService;
using WatchThat.Service.UserService;
using WatchThat.Service.MovieService;
using WatchThat.Repository.ReviewRepository;
using WatchThat.Service.ReviewService;
using DinkToPdf.Contracts;
using DinkToPdf;
using WatchThat.Service.PdfService;

namespace WatchThat.Api
{
    public static class AddCustomServices
    {
        public static void ConfigureCustomServices(this IServiceCollection services)
        {
            services.AddScoped<IAuthenticationService, AuthenticationServiceImpl>();

            services.AddScoped<IUserRepository, UserRepositoryImpl>();
            services.AddScoped<IUserService, UserServiceImpl>();

            services.AddScoped<IDirectorRepository, DirectorRepositoryImpl>();
            services.AddScoped<IDirectorService, DirectorServiceImpl>();

            services.AddScoped<IActorRepository, ActorRepositoryImpl>();
            services.AddScoped<IActorService, ActorServiceImpl>();

            services.AddScoped<IGenreRepository, GenreRepositoryImpl>();
            services.AddScoped<IGenreService, GenreServiceImpl>();

            services.AddScoped<IMovieRepository, MovieRepositoryImpl>();
            services.AddScoped<IMovieService, MovieServiceImpl>();

            services.AddScoped<IReviewRepository, ReviewRepositoryImpl>();
            services.AddScoped<IReviewService, ReviewServiceImpl>();

            services.AddSingleton(typeof(IConverter), new SynchronizedConverter(new PdfTools()));
            services.AddScoped<IPdfService, PdfServiceImpl>();
        }
    }
}
