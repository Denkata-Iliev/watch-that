﻿using LazyCache;
using System.Net;
using WatchThat.Entity;
using WatchThat.Infrastructure.Exceptions;
using WatchThat.Repository.BaseRepository;

namespace WatchThat.Service.BaseService
{
    public class BaseServiceImpl<T> : IBaseService<T> where T : BaseEntity
    {
        private readonly IBaseRepository<T> baseRepository;
        private readonly IAppCache cache;

        protected BaseServiceImpl(IBaseRepository<T> baseRepository, IAppCache cache)
        {
            this.baseRepository = baseRepository;
            this.cache = cache;
        }

        public T GetById(int id)
        {
            T entity = baseRepository.GetById(id);

            if (entity == null)
            {
                throw new ResourceNotFoundException($"{typeof(T).Name} with id {id} doesn't exist!");
            }

            return entity;
        }

        public IEnumerable<T> GetPage(int page, int size)
        {
            if (page * size > 100)
            {
                return baseRepository.GetPage(page, size);
            }

            return GetCached()
                .Skip((page - 1) * size)
                .Take(size);
        }

        public int GetCount()
        {
            return baseRepository.GetCount();
        }

        public void Create(T entity)
        {
            baseRepository.Create(entity);
            cache.Remove(typeof(T).Name);
        }

        public void Update(T entity)
        {
            baseRepository.Update(entity);
            cache.Remove(typeof(T).Name);
        }

        public void Delete(int id)
        {
            baseRepository.Delete(GetById(id));
            cache.Remove(typeof(T).Name);
        }

        private IEnumerable<T> GetCached()
        {
            if (cache.TryGetValue(typeof(T).Name, out IEnumerable<T> result))
            {
                return result;
            }

            IEnumerable<T> entities = baseRepository.GetAll().Take(100);

            cache.Add(typeof(T).Name, entities, DateTimeOffset.Now.AddMinutes(5));
            return entities;
        }
    }
}
