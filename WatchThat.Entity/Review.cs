﻿namespace WatchThat.Entity
{
    public class Review : BaseEntity
    {
        public int MovieId { get; set; }
        public Movie Movie { get; set; }

        public string MovieReview { get; set; }

        public int UserId { get; set; }
        public User Author { get; set; }
    }
}
