﻿using Microsoft.EntityFrameworkCore;
using WatchThat.Entity;

namespace WatchThat.Repository
{
    public class WatchThatContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Actor> Actors { get; set; }
        public DbSet<Director> Directors { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Review> Reviews { get; set; }

        public WatchThatContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // seed default admin
            modelBuilder.Entity<User>()
                .HasData(new User
                {
                    Id = 1,
                    Username = "DefaultAdmin",
                    Password = "$2a$12$XPNOefgd94Cks4K7bZ1L.ubVFGUMjHgjaUaBF5.3N56mIVg5ubHm2",
                    Role = Role.ADMIN
                });

            // save Role enum as string in db
            modelBuilder.Entity<User>()
                .Property(u => u.Role)
                .HasConversion<string>()
                .HasColumnType("nvarchar(max)");
                
            // make username unique
            modelBuilder.Entity<User>()
                .HasIndex(u => u.Username)
                .IsUnique();

            // make genre name unique
            modelBuilder.Entity<Genre>()
                .HasIndex(g => g.Name)
                .IsUnique();

            modelBuilder.Entity<Review>()
                .HasIndex(r => new { r.UserId, r.MovieId })
                .IsUnique();

            // auto include movie properties
            modelBuilder.Entity<Movie>()
                .Navigation(m => m.Director)
                .AutoInclude();

            modelBuilder.Entity<Movie>()
                .Navigation(m => m.Genres)
                .AutoInclude();

            modelBuilder.Entity<Movie>()
                .Navigation(m => m.Cast)
                .AutoInclude();

            modelBuilder.Entity<Review>()
                .Navigation(r => r.Movie)
                .AutoInclude();

            modelBuilder.Entity<Review>()
                .Navigation(r => r.Author)
                .AutoInclude();

            base.OnModelCreating(modelBuilder);
        }
    }
}
