﻿namespace WatchThat.Entity
{
    public abstract class Person : NameEntity
    {
        public int YearOfBirth { get; set; }

        public int Age { get; set; }

        public Person(string name, int yearOfBirth) : base(name)
        {
            YearOfBirth = yearOfBirth;
            Age = DateTime.Now.Year - yearOfBirth;
        }
    }
}
