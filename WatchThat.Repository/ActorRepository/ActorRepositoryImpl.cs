﻿using Microsoft.EntityFrameworkCore;
using WatchThat.Entity;
using WatchThat.Repository.BaseRepository;

namespace WatchThat.Repository.ActorRepository
{
    public class ActorRepositoryImpl : BaseRepositoryImpl<Actor>, IActorRepository
    {
        private readonly DbSet<Actor> actors;

        public ActorRepositoryImpl(WatchThatContext context) : base(context)
        {
            actors = context.Set<Actor>();
        }
    }
}
