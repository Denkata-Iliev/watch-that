﻿using System.Net;

namespace WatchThat.Infrastructure.Exceptions
{
    public class ReviewAlreadyExistsException : CustomHttpException
    {
        public ReviewAlreadyExistsException(string? message) : base(message, HttpStatusCode.Conflict)
        {
        }
    }
}
