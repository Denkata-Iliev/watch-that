﻿using WatchThat.Dtos;
using WatchThat.Dtos.ReviewDtos;
using WatchThat.Entity;
using WatchThat.Service.BaseService;

namespace WatchThat.Service.ReviewService
{
    public interface IReviewService : IBaseService<Review>
    {
        ReviewResponseDto Create(ReviewCreateDto reviewCreateDto);

        new ReviewResponseDto GetById(int id);

        new PaginationDto<ReviewResponseDto> GetPage(int page, int size);

        ReviewResponseDto Update(int id, ReviewUpdateDto reviewUpdateDto);
    }
}
