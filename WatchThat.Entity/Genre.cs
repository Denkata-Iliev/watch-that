﻿namespace WatchThat.Entity
{
    public class Genre : NameEntity
    {
        public ISet<Movie> Movies { get; set; }

        public Genre(string name) : base(name)
        {
        }
    }
}
