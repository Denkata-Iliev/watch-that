﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WatchThat.Repository.Migrations
{
    public partial class SeedDefaultAdmin : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Password", "Role", "Username" },
                values: new object[] { 1, "$2a$12$XPNOefgd94Cks4K7bZ1L.ubVFGUMjHgjaUaBF5.3N56mIVg5ubHm2", "ADMIN", "DefaultAdmin" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}
