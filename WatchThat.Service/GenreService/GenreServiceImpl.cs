﻿using AutoMapper;
using LazyCache;
using System.Net;
using WatchThat.Dtos;
using WatchThat.Dtos.GenreDtos;
using WatchThat.Entity;
using WatchThat.Infrastructure.Exceptions;
using WatchThat.Repository.GenreRepository;
using WatchThat.Service.BaseService;

namespace WatchThat.Service.GenreService
{
    public class GenreServiceImpl : BaseServiceImpl<Genre>, IGenreService
    {
        private readonly IGenreRepository genreRepository;
        private readonly IMapper mapper;

        public GenreServiceImpl(IGenreRepository genreRepository,
                                IAppCache cache,
                                IMapper mapper) : base(genreRepository, cache)
        {
            this.genreRepository = genreRepository;
            this.mapper = mapper;
        }

        public GenreResponseDto Create(GenreCreateDto createDto)
        {
            ValidateGenreNameIsUnique(createDto.Name);

            Genre genre = mapper.Map<Genre>(createDto);

            Create(genre);

            return mapper.Map<GenreResponseDto>(genre);
        }

        public new GenreResponseDto GetById(int id)
        {
            Genre genre = base.GetById(id);
            return mapper.Map<GenreResponseDto>(genre);
        }

        public new PaginationDto<GenreResponseDto> GetPage(int page, int size)
        {
            decimal decimalSize = Convert.ToDecimal(size);
            int totalElements = GetCount();
            PaginationDto<GenreResponseDto> pagination = new PaginationDto<GenreResponseDto>
            {
                Page = page,
                Size = size,
                Content = base.GetPage(page, size).Select(mapper.Map<GenreResponseDto>),
                PageCount = Math.Ceiling(totalElements / decimalSize),
                TotalElements = totalElements
            };

            return pagination;
        }

        public GenreResponseDto Update(int id, GenreCreateDto dto)
        {
            Genre genre = base.GetById(id);

            ValidateGenreNameIsUnique(dto.Name);

            mapper.Map(dto, genre);

            Update(genre);

            return mapper.Map<GenreResponseDto>(genre);
        }

        private void ValidateGenreNameIsUnique(string name)
        {
            if (genreRepository.ExistsByName(name))
            {
                throw new UniqueKeyConflictException("This genre already exists!");
            }
        }

        public Genre GetEntityById(int id)
        {
            return base.GetById(id);
        }

        public Genre GetEntityByName(string name)
        {
            return genreRepository.GetAll().First(g => g.Name == name);
        }
    }
}
