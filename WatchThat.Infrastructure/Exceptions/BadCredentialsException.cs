﻿using System.Net;

namespace WatchThat.Infrastructure.Exceptions
{
    public class BadCredentialsException : CustomHttpException
    {
        public BadCredentialsException(string? message) : base(message, HttpStatusCode.BadRequest)
        {
        }
    }
}
