﻿using AutoMapper;
using LazyCache;
using WatchThat.Dtos;
using WatchThat.Dtos.ActorDtos;
using WatchThat.Entity;
using WatchThat.Repository.ActorRepository;
using WatchThat.Service.BaseService;

namespace WatchThat.Service.ActorService
{
    public class ActorServiceImpl : BaseServiceImpl<Actor>, IActorService
    {
        private readonly IActorRepository actorRepository;
        private readonly IMapper mapper;

        public ActorServiceImpl(IActorRepository actorRepository,
                                IAppCache cache,
                                IMapper mapper) : base(actorRepository, cache)
        {
            this.actorRepository = actorRepository;
            this.mapper = mapper;
        }

        public ActorResponseDto Create(ActorCreateDto createDto)
        {
            Actor actor = mapper.Map<Actor>(createDto);

            Create(actor);

            return mapper.Map<ActorResponseDto>(actor);
        }

        public ActorResponseDto Update(ActorCreateDto dto, int id)
        {
            Actor actor = base.GetById(id);

            mapper.Map(dto, actor);
            actor.Age = DateTime.Now.Year - actor.YearOfBirth;

            Update(actor);

            return mapper.Map<ActorResponseDto>(actor);
        }

        public new PaginationDto<ActorResponseDto> GetPage(int page, int size)
        {
            decimal decimalSize = Convert.ToDecimal(size);
            int totalElements = GetCount();
            PaginationDto<ActorResponseDto> pagination = new PaginationDto<ActorResponseDto>
            {
                Page = page,
                Size = size,
                Content = base.GetPage(page, size).Select(mapper.Map<ActorResponseDto>),
                PageCount = Math.Ceiling(totalElements / decimalSize),
                TotalElements = totalElements
            };

            return pagination;
        }

        public new ActorResponseDto GetById(int id)
        {
            return mapper.Map<ActorResponseDto>(base.GetById(id));
        }

        public Actor GetEntityById(int id)
        {
            return base.GetById(id);
        }
    }
}
