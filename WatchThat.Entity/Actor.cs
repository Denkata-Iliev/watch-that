﻿namespace WatchThat.Entity
{
    public class Actor : Person
    {
        public ISet<Movie> Movies { get; set; }

        public Actor(string name, int yearOfBirth) : base(name, yearOfBirth)
        {
        }
    }
}
