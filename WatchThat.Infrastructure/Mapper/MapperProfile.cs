﻿using AutoMapper;
using WatchThat.Dtos.ActorDtos;
using WatchThat.Dtos.Authentication;
using WatchThat.Dtos.DirectorDtos;
using WatchThat.Dtos.GenreDtos;
using WatchThat.Dtos.MovieDtos;
using WatchThat.Dtos.ReviewDtos;
using WatchThat.Dtos.UserDtos;
using WatchThat.Entity;

namespace WatchThat.Infrastructure.Mapper
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            //authentication
            CreateMap<RegisterDto, User>().ConvertUsing(typeof(BCryptPasswordHasher));
            CreateMap<User, UserResponseDto>();

            //user
            CreateMap<UserUpdateDto, User>().ConvertUsing(typeof(BCryptPasswordHasher));

            //director
            CreateMap<Director, DirectorResponseDto>();
            CreateMap<DirectorCreateDto, Director>();

            //actor
            CreateMap<Actor, ActorResponseDto>();
            CreateMap<ActorCreateDto, Actor>();

            //genre
            CreateMap<Genre, GenreResponseDto>();
            CreateMap<GenreCreateDto, Genre>();

            //movie
            CreateMap<MovieCreateDto, Movie>();
            CreateMap<Movie, MovieResponseDto>();

            //review
            CreateMap<ReviewCreateDto, Review>();
            CreateMap<Review, ReviewResponseDto>();
            CreateMap<ReviewUpdateDto, Review>();
        }
    }
}