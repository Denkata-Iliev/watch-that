﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Text;
using WatchThat.Dtos.Authentication;
using WatchThat.Entity;
using WatchThat.Infrastructure.Exceptions;
using WatchThat.Service.UserService;

namespace WatchThat.Service.AuthenticationService
{
    public class AuthenticationServiceImpl : IAuthenticationService
    {
        private const string WrongUsernameOrPass = "Wrong username or password";

        private readonly IUserService userService;
        private readonly IMapper mapper;
        private readonly IConfiguration configuration;

        public AuthenticationServiceImpl(IUserService userService, IMapper mapper, IConfiguration configuration)
        {
            this.userService = userService;
            this.mapper = mapper;
            this.configuration = configuration;
        }

        public UserResponseDto Register(RegisterDto register)
        {
            if (userService.ExistsByUsername(register.Username))
            {
                throw new UniqueKeyConflictException("This username is taken!");
            }

            User user = mapper.Map<RegisterDto, User>(register);

            userService.Create(user);

            return mapper.Map<User, UserResponseDto>(user);
        }

        public LoginResponseDto Login(LoginDto login)
        {
            if (!userService.ExistsByUsername(login.Username))
            {
                throw new AuthException(WrongUsernameOrPass, HttpStatusCode.Unauthorized);
            }

            User user = userService.GetByUsername(login.Username);

            if (!BCrypt.Net.BCrypt.Verify(login.Password, user.Password))
            {
                throw new AuthException(WrongUsernameOrPass, HttpStatusCode.Unauthorized);
            }

            return new LoginResponseDto(CreateJwtToken(user));
        }

        private string CreateJwtToken(User user)
        {
            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.Username),
                new Claim(ClaimTypes.Role, user.Role.ToString())
            };

            SymmetricSecurityKey key = new SymmetricSecurityKey(
                Encoding.UTF8.GetBytes(
                    configuration.GetSection("AppSettings:TokenSecret").Value
                    )
                );

            SigningCredentials credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            JwtSecurityToken token = new JwtSecurityToken(
                claims: claims,
                signingCredentials: credentials,
                expires: DateTime.Now.AddHours(1)
                );

            string jwtToken = new JwtSecurityTokenHandler().WriteToken(token);

            return jwtToken;
        }
    }
}
