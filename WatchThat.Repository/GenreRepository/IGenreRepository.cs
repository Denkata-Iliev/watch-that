﻿using WatchThat.Entity;
using WatchThat.Repository.BaseRepository;

namespace WatchThat.Repository.GenreRepository
{
    public interface IGenreRepository : IBaseRepository<Genre>
    {
        bool ExistsByName(string name);
    }
}
