﻿using AutoMapper;
using LazyCache;
using Microsoft.AspNetCore.Http;
using System.Net;
using WatchThat.Dtos;
using WatchThat.Dtos.Authentication;
using WatchThat.Dtos.UserDtos;
using WatchThat.Entity;
using WatchThat.Infrastructure.Exceptions;
using WatchThat.Repository.UserRepository;
using WatchThat.Service.BaseService;

namespace WatchThat.Service.UserService
{
    public class UserServiceImpl : BaseServiceImpl<User>, IUserService
    {
        private const string NoPermissionForThat = "You don't have permission to do that!";

        private readonly IUserRepository userRepository;
        private readonly IAppCache cache;
        private readonly IMapper mapper;
        private readonly IHttpContextAccessor httpContextAccessor;

        public UserServiceImpl(IUserRepository userRepository,
                                IAppCache cache,
                                IMapper mapper,
                                IHttpContextAccessor httpContextAccessor) : base(userRepository, cache)
        {
            this.userRepository = userRepository;
            this.cache = cache;
            this.mapper = mapper;
            this.httpContextAccessor = httpContextAccessor;
        }

        public UserResponseDto CreateAdmin(RegisterDto register)
        {
            if (ExistsByUsername(register.Username))
            {
                throw new UniqueKeyConflictException("This username is taken!");
            }

            User user = mapper.Map<RegisterDto, User>(register);
            user.Role = Role.ADMIN;

            Create(user);

            return mapper.Map<UserResponseDto>(user);
        }

        public new PaginationDto<UserResponseDto> GetPage(int page, int size)
        {
            decimal decimalSize = Convert.ToDecimal(size);
            int totalElements = GetCount();
            PaginationDto<UserResponseDto> pagination = new PaginationDto<UserResponseDto>
            {
                Page = page,
                Size = size,
                Content = base.GetPage(page, size).Select(mapper.Map<UserResponseDto>),
                PageCount = Math.Ceiling(totalElements / decimalSize),
                TotalElements = totalElements
            };

            return pagination;
        }

        public new UserResponseDto GetById(int id)
        {
            User user = base.GetById(id);

            ValidateSameUserOrAdmin(user.Username);

            return mapper.Map<UserResponseDto>(user);
        }

        public UserResponseDto Update(UserUpdateDto updateDto, string username)
        {
            ValidateSameUserOrAdmin(username);

            User user = GetByUsername(username);
            if (!BCrypt.Net.BCrypt.Verify(updateDto.OldPassword, user.Password))
            {
                throw new BadCredentialsException("Wrong password!");
            }

            mapper.Map(updateDto, user);
            Update(user);

            return mapper.Map<UserResponseDto>(user);
        }

        public UserResponseDto ChangeRole(string username, RoleChangeDto roleChange)
        {
            User user = GetByUsername(username);

            if (user.Role == Role.ADMIN && userRepository.IsLastAdmin())
            {
                throw new LastAdminException("You can't change the role of the last admin!");
            }

            user.Role = roleChange.Role;

            Update(user);

            return mapper.Map<UserResponseDto>(user);
        }

        public new void Delete(int id)
        {
            User user = base.GetById(id);

            if (user.Role == Role.ADMIN && userRepository.IsLastAdmin())
            {
                throw new LastAdminException("You can't delete the last admin!");
            }

            userRepository.Delete(user);
            cache.Remove(nameof(User));
        }

        public bool ExistsByUsername(string username)
        {
            return userRepository.ExistsByUsername(username);
        }

        public User GetByUsername(string username)
        {
            if (!ExistsByUsername(username))
            {
                throw new ResourceNotFoundException("User with this username doesn't exist!");
            }

            User user = userRepository.GetByUsername(username);

            return user;
        }

        private void ValidateSameUserOrAdmin(string username)
        {
            if (httpContextAccessor.HttpContext.User?.Identity?.Name != username
                && !httpContextAccessor.HttpContext.User.IsInRole(Role.ADMIN.ToString()))
            {
                throw new AuthException(NoPermissionForThat, HttpStatusCode.Forbidden);
            }
        }
    }
}
