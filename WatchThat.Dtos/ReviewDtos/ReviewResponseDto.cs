﻿using WatchThat.Dtos.Authentication;
using WatchThat.Dtos.MovieDtos;

namespace WatchThat.Dtos.ReviewDtos
{
    public class ReviewResponseDto
    {
        public int Id { get; set; }

        public MovieResponseDto Movie { get; set; }

        public string MovieReview { get; set; }

        public UserResponseDto Author { get; set; }
    }
}
