﻿using System.Net;

namespace WatchThat.Infrastructure.Exceptions
{
    public class LastAdminException : CustomHttpException
    {
        public LastAdminException(string? message) : base(message, HttpStatusCode.BadRequest)
        {
        }
    }
}
