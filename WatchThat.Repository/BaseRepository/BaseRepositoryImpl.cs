﻿using Microsoft.EntityFrameworkCore;
using WatchThat.Entity;

namespace WatchThat.Repository.BaseRepository
{
    public class BaseRepositoryImpl<T> : IBaseRepository<T> where T : BaseEntity
    {
        private readonly WatchThatContext context;
        private readonly DbSet<T> dbSet;

        protected BaseRepositoryImpl(WatchThatContext context)
        {
            this.context = context;
            dbSet = context.Set<T>();
        }

        public T GetById(int id)
        {
            return dbSet.Find(id);
        }

        public ICollection<T> GetAll()
        {
            return dbSet.ToList();
        }

        public IEnumerable<T> GetPage(int page, int size)
        {
            return dbSet
                .Skip((page - 1) * size)
                .Take(size);
        }

        public int GetCount()
        {
            return dbSet.Count();
        }

        public void Create(T entity)
        {
            dbSet.Add(entity);
            context.SaveChanges();
        }

        public void Update(T entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();
        }

        public void Delete(T entity)
        {
            dbSet.Remove(entity);
            context.SaveChanges();
        }
    }
}
