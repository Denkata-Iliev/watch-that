﻿using System.ComponentModel.DataAnnotations;

namespace WatchThat.Dtos.ReviewDtos
{
    public class ReviewUpdateDto
    {
        [Required]
        [MinLength(5, ErrorMessage = "Review must be at least 5 characters long")]
        public string MovieReview { get; set; }
    }
}
