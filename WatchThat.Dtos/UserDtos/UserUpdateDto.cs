﻿using System.ComponentModel.DataAnnotations;

namespace WatchThat.Dtos.UserDtos
{
    public class UserUpdateDto
    {
        [Required]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "Password is required!")]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&#+\-=^_()/\\.,<>""':;])[A-Za-z\d@$!%*?&#+\-=^_()/\\.,<>""':;]{8,}$",
            ErrorMessage = "Password must be at least 8 characters long. " +
            "It must contain at least one uppercase letter, one lowercase letter, " +
            "one number and one special character.")]
        public string NewPassword { get; set; }

        [Compare(nameof(NewPassword), ErrorMessage = "New password doesn't match!")]
        public string ConfirmNewPassword { get; set; }
    }
}
