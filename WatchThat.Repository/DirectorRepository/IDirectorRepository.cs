﻿using WatchThat.Entity;
using WatchThat.Repository.BaseRepository;

namespace WatchThat.Repository.DirectorRepository
{
    public interface IDirectorRepository : IBaseRepository<Director>
    {
    }
}
