﻿using System.ComponentModel.DataAnnotations;

namespace WatchThat.Dtos.ActorDtos
{
    public class ActorCreateDto
    {
        [MinLength(3, ErrorMessage = "Name must be at least 3 characters long")]
        public string Name { get; set; }

        [Range(1950, 2006)]
        public int YearOfBirth { get; set; }
    }
}
