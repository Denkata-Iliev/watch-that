﻿using WatchThat.Dtos;
using WatchThat.Dtos.DirectorDtos;
using WatchThat.Entity;
using WatchThat.Service.BaseService;

namespace WatchThat.Service.DirectorService
{
    public interface IDirectorService : IBaseService<Director>
    {
        DirectorResponseDto Create(DirectorCreateDto createDto);

        new PaginationDto<DirectorResponseDto> GetPage(int page, int size);

        new DirectorResponseDto GetById(int id);

        DirectorResponseDto Update(DirectorCreateDto dto, int id);

        Director GetEntityById(int id);
    }
}
