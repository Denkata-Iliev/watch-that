﻿using Microsoft.EntityFrameworkCore;
using WatchThat.Entity;
using WatchThat.Repository.BaseRepository;

namespace WatchThat.Repository.MovieRepository
{
    public class MovieRepositoryImpl : BaseRepositoryImpl<Movie>, IMovieRepository
    {
        private readonly DbSet<Movie> movies;

        public MovieRepositoryImpl(WatchThatContext context) : base(context)
        {
            movies = context.Set<Movie>();
        }
    }
}
