﻿using WatchThat.Dtos.ActorDtos;
using WatchThat.Dtos.DirectorDtos;
using WatchThat.Dtos.GenreDtos;

namespace WatchThat.Dtos.MovieDtos
{
    public class MovieResponseDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public decimal Budget { get; set; }

        public decimal Earnings { get; set; }

        public int Duration { get; set; }

        public int PublishedYear { get; set; }

        public DirectorResponseDto Director { get; set; }

        public ICollection<ActorResponseDto> Cast { get; set; }

        public ICollection<GenreResponseDto> Genres { get; set; }
    }
}
