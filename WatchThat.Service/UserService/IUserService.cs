﻿using WatchThat.Dtos;
using WatchThat.Dtos.Authentication;
using WatchThat.Dtos.UserDtos;
using WatchThat.Entity;
using WatchThat.Service.BaseService;

namespace WatchThat.Service.UserService
{
    public interface IUserService : IBaseService<User>
    {
        UserResponseDto CreateAdmin(RegisterDto register);

        new UserResponseDto GetById(int id);

        UserResponseDto Update(UserUpdateDto updateDto, string username);

        bool ExistsByUsername(string username);

        User GetByUsername(string username);

        new PaginationDto<UserResponseDto> GetPage(int page, int size);

        new void Delete(int id);

        UserResponseDto ChangeRole(string username, RoleChangeDto roleChange);
    }
}
