﻿using WatchThat.Entity;
using WatchThat.Repository.BaseRepository;

namespace WatchThat.Repository.UserRepository
{
    public interface IUserRepository : IBaseRepository<User>
    {
        bool ExistsByUsername(string username);

        bool IsLastAdmin();

        User GetByUsername(string username);
    }
}
