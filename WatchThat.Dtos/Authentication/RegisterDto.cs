﻿using System.ComponentModel.DataAnnotations;

namespace WatchThat.Dtos.Authentication
{
    public class RegisterDto
    {
        [Required(ErrorMessage = "Username is required!")]
        [MinLength(3, ErrorMessage = "Username must be at least 3 characters long!")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password is required!")]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&#+\-=^_()/\\.,<>""':;])[A-Za-z\d@$!%*?&#+\-=^_()/\\.,<>""':;]{8,}$",
            ErrorMessage = "Password must be at least 8 characters long. " +
            "It must contain at least one uppercase letter, one lowercase letter, " +
            "one number and one special character.")]
        public string Password { get; set; }

        [Compare(nameof(Password), ErrorMessage = "Passwords don't match!")]
        public string ConfirmPassword { get; set; }
    }
}