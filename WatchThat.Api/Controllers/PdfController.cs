﻿using Microsoft.AspNetCore.Mvc;
using WatchThat.Service.PdfService;

namespace WatchThat.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PdfController : ControllerBase
    {
        private readonly IPdfService pdfServcice;

        public PdfController(IPdfService pdfServcice)
        {
            this.pdfServcice = pdfServcice;
        }

        [HttpGet]
        public IActionResult GetPdf()
        {
            pdfServcice.CreatePdf();
            return Ok("Successfully created PDF document.");
        }
    }
}
