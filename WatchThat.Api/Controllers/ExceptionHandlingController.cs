﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using WatchThat.Infrastructure.Exceptions;

namespace WatchThat.Api.Controllers
{
    [ApiController]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ExceptionHandlingController : ControllerBase
    {
        [Route("error")]
        public object Error()
        {
            var context = HttpContext.Features.Get<IExceptionHandlerFeature>();
            Exception exception = context.Error;
            int code = 500;

            if (exception is CustomHttpException httpException)
            {
                code = (int)httpException.StatusCode;
            }

            Response.StatusCode = code;

            return new { exception.Message, code };
        }
    }
}
