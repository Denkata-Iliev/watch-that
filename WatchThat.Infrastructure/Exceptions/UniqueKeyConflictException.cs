﻿using System.Net;

namespace WatchThat.Infrastructure.Exceptions
{
    public class UniqueKeyConflictException : CustomHttpException
    {
        public UniqueKeyConflictException(string? message) : base(message, HttpStatusCode.Conflict)
        {
        }
    }
}
