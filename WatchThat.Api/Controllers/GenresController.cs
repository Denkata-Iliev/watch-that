﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WatchThat.Dtos.GenreDtos;
using WatchThat.Service.GenreService;

namespace WatchThat.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GenresController : ControllerBase
    {
        private readonly IGenreService genreService;

        public GenresController(IGenreService genreService)
        {
            this.genreService = genreService;
        }

        [HttpPost]
        [Authorize(Roles = "ADMIN")]
        public IActionResult Create([FromBody] GenreCreateDto createDto)
        {
            return Ok(genreService.Create(createDto));
        }

        [HttpGet]
        [Authorize]
        public IActionResult GetPage([FromQuery] int page = 1, [FromQuery] int size = 5)
        {
            return Ok(genreService.GetPage(page, size));
        }

        [HttpGet("{id}")]
        [Authorize]
        public IActionResult GetById([FromRoute] int id)
        {
            return Ok(genreService.GetById(id));
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "ADMIN")]
        public IActionResult Update([FromRoute] int id, [FromBody] GenreCreateDto dto)
        {
            return Ok(genreService.Update(id, dto));
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "ADMIN")]
        public IActionResult Delete([FromRoute] int id)
        {
            genreService.Delete(id);
            return NoContent();
        }
    }
}
