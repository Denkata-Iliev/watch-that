﻿namespace WatchThat.Entity
{
    public class Director : Person
    {
        public ISet<Movie> Movies { get; set; }

        public Director(string name, int yearOfBirth) : base(name, yearOfBirth)
        {
        }
    }
}
