﻿namespace WatchThat.Dtos.GenreDtos
{
    public class GenreResponseDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
