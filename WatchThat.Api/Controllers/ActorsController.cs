﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WatchThat.Dtos.ActorDtos;
using WatchThat.Service.ActorService;

namespace WatchThat.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActorsController : ControllerBase
    {
        private readonly IActorService actorService;

        public ActorsController(IActorService actorService)
        {
            this.actorService = actorService;
        }

        [HttpPost]
        [Authorize(Roles = "ADMIN")]
        public IActionResult Create([FromBody] ActorCreateDto createDto)
        {
            return Ok(actorService.Create(createDto));
        }

        [HttpGet]
        [Authorize]
        public IActionResult GetPage([FromQuery] int page = 1, [FromQuery] int size = 5)
        {
            return Ok(actorService.GetPage(page, size));
        }

        [HttpGet("{id}")]
        [Authorize]
        public IActionResult GetById([FromRoute] int id)
        {
            return Ok(actorService.GetById(id));
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "ADMIN")]
        public IActionResult Update([FromRoute] int id, [FromBody] ActorCreateDto dto)
        {
            return Ok(actorService.Update(dto, id));
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "ADMIN")]
        public IActionResult Delete([FromRoute] int id)
        {
            actorService.Delete(id);
            return NoContent();
        }
    }
}
