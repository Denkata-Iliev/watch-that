﻿using WatchThat.Entity;

namespace WatchThat.Dtos.UserDtos
{
    public class RoleChangeDto
    {
        public Role Role { get; set; }
    }
}
