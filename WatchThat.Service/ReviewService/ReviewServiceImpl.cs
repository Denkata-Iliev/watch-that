﻿using AutoMapper;
using LazyCache;
using Microsoft.AspNetCore.Http;
using System.Net;
using WatchThat.Dtos;
using WatchThat.Dtos.ReviewDtos;
using WatchThat.Entity;
using WatchThat.Infrastructure.Exceptions;
using WatchThat.Repository.ReviewRepository;
using WatchThat.Service.BaseService;
using WatchThat.Service.MovieService;
using WatchThat.Service.UserService;

namespace WatchThat.Service.ReviewService
{
    public class ReviewServiceImpl : BaseServiceImpl<Review>, IReviewService
    {
        private const string NoPermissionForThat = "You don't have permission to do that!";

        private readonly IReviewRepository reviewRepository;
        private readonly IMapper mapper;
        private readonly IHttpContextAccessor httpContext;
        private readonly IUserService userService;
        private readonly IMovieService movieService;

        public ReviewServiceImpl(IReviewRepository reviewRepository,
                                 IMapper mapper,
                                 IHttpContextAccessor httpContext,
                                 IAppCache cache,
                                 IUserService userService,
                                 IMovieService movieService) : base(reviewRepository, cache)
        {
            this.reviewRepository = reviewRepository;
            this.mapper = mapper;
            this.httpContext = httpContext;
            this.userService = userService;
            this.movieService = movieService;
        }

        public ReviewResponseDto Create(ReviewCreateDto reviewCreateDto)
        {
            Review review = mapper.Map<Review>(reviewCreateDto);

            review.UserId = GetUserId();

            review.Movie = movieService.GetEntityById(reviewCreateDto.MovieId);

            try
            {
                Create(review);
            }
            catch (Exception)
            {
                throw new ReviewAlreadyExistsException("You've already written a review for this movie!");
            }

            return mapper.Map<ReviewResponseDto>(review);
        }

        public new ReviewResponseDto GetById(int id)
        {
            return mapper.Map<ReviewResponseDto>(base.GetById(id));
        }

        public new PaginationDto<ReviewResponseDto> GetPage(int page, int size)
        {
            decimal decimalSize = Convert.ToDecimal(size);
            int totalElements = GetCount();
            PaginationDto<ReviewResponseDto> pagination = new PaginationDto<ReviewResponseDto>
            {
                Page = page,
                Size = size,
                Content = base.GetPage(page, size).Select(mapper.Map<ReviewResponseDto>),
                PageCount = Math.Ceiling(totalElements / decimalSize),
                TotalElements = totalElements
            };

            return pagination;
        }

        public ReviewResponseDto Update(int id, ReviewUpdateDto reviewUpdateDto)
        {
            Review review = base.GetById(id);

            if (GetUserId() != review.UserId && !UserIsInRole(Role.ADMIN.ToString()))
            {
                throw new AuthException(NoPermissionForThat, HttpStatusCode.Forbidden);
            }

            mapper.Map(reviewUpdateDto, review);

            Update(review);

            return mapper.Map<ReviewResponseDto>(review);
        }

        public new void Delete(int id)
        {
            Review review = base.GetById(id);

            if (GetUserId() != review.UserId && !UserIsInRole(Role.ADMIN.ToString()))
            {
                throw new AuthException(NoPermissionForThat, HttpStatusCode.Forbidden);
            }

            reviewRepository.Delete(review);
        }

        private int GetUserId()
        {
            return userService.GetByUsername(httpContext.HttpContext.User.Identity.Name).Id;
        }

        private bool UserIsInRole(string role)
        {
            return httpContext.HttpContext.User.IsInRole(role);
        }
    }
}
